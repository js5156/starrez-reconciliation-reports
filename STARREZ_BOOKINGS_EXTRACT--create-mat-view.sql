create MATERIALIZED VIEW starrez_bookings_extract_vw
    REFRESH
        complete
        START WITH trunc(SYSDATE) + 19/24
        NEXT SYSDATE + 1
AS
    SELECT *
    FROM STARREZ_BOOKING_EXTRACT@STARREZ.TEACHERS;

CREATE or replace PUBLIC SYNONYM starrez_bookings_extract_vw FOR baninst1.starrez_bookings_extract_vw;