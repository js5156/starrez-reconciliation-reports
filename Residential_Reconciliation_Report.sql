/* NOTES
Starrez Statuses to SLRRASG_ASCD_CODE Xwalk:
        RESV = 'AC'
        INRM = 'AC'
        CNCL = 'CA'
        HIST = 'MO'
*/
WITH cte_curr_term_dates AS
(SELECT
  stvterm_code,
  stvterm_housing_start_date,
  stvterm_housing_end_date
      from stvterm
      where  substr(stvterm_code,5,2) NOT IN ('03','07','11')
      AND stvterm_code <> '999999'
      AND stvterm_code = (SELECT current_term_code FROM tcapp.selterm)),
cte_next_term_dates AS
(SELECT
  stvterm_code,
  stvterm_housing_start_date,
  stvterm_housing_end_date
      from stvterm
      where  substr(stvterm_code,5,2) NOT IN ('03','07','11')
      AND stvterm_code <> '999999'
      AND stvterm_code = (SELECT next_term FROM tcapp.selterm))
select id4 as pidm,
        f_getspridenid(id4) as "ID",
        trim(TermSessionCode) as term_cd,
        CheckInDate,
        checkoutdate,
        customstring11 as bldg_code,
        (select stvbldg_desc from stvbldg where stvbldg_code = customstring11) bldg_desc,
        decode(LENGTH(CustomString2),1,lpad(CustomString2,2,0),CustomString2) room_number,
        ABBREVIATION as Status,
        DateModified,
        TermSessionID,
        DateCreated,
        BookingID,
 (case 
        WHEN NOT EXISTS (
            SELECT 'x' 
            FROM slrrasg 
            WHERE slrrasg_pidm = starrez_extract.id4        
                 and slrrasg_begin_date = CASE WHEN slrrasg_term_code = (SELECT current_term_code FROM tcapp.selterm)
                                                                    THEN
                                                                            CASE WHEN CheckInDate >= (SELECT stvterm_housing_start_date FROM cte_curr_term_dates)
                                                                                  THEN CheckInDate
                                                                                  when checkindate < (select stvterm_housing_start_date from cte_curr_term_dates)
                                                                                  then (select stvterm_housing_start_date from cte_curr_term_dates ) END
                                                                  WHEN slrrasg_term_code = (SELECT next_term FROM tcapp.selterm)
                                                                     then
                                                                            CASE WHEN CheckInDate >= (SELECT stvterm_housing_start_date FROM cte_next_term_dates)
                                                                                  THEN CheckInDate
                                                                                  when checkindate < (select stvterm_housing_start_date from cte_next_term_dates)
                                                                                  then (select stvterm_housing_start_date from cte_next_term_dates ) END                        
                                                       END
                 and slrrasg_end_date = CASE WHEN slrrasg_term_code = (SELECT current_term_code FROM tcapp.selterm)
                                                                    THEN
                                                                            CASE WHEN CheckOutDate < (SELECT stvterm_housing_end_date FROM cte_curr_term_dates)
                                                                                  THEN CheckOutDate
                                                                                  WHEN CheckOutDate >= (SELECT stvterm_housing_end_date FROM cte_curr_term_dates)
                                                                                  THEN (SELECT stvterm_housing_end_date FROM cte_curr_term_dates) END
                                                                  WHEN slrrasg_term_code = (SELECT next_term FROM tcapp.selterm)
                                                                     then
                                                                            CASE WHEN CheckOutDate < (SELECT stvterm_housing_end_date FROM cte_next_term_dates)
                                                                                  then checkoutdate
                                                                                  when checkoutdate >= (select stvterm_housing_end_date from cte_next_term_dates)
                                                                                  THEN (SELECT stvterm_housing_end_date FROM cte_next_term_dates) END                        
                                                     END                                                         
                AND slrrasg_ascd_code = 'AC'
                AND CUSTOMSTRING11 = slrrasg_bldg_code
                AND decode(LENGTH(CustomString2),1,lpad(CustomString2,2,0),CustomString2) = slrrasg.slrrasg_room_number
                                    )
                and trim(termsessioncode) = 'NOTERM'         
        then 'Y'
                WHEN NOT EXISTS (
                                                    SELECT 'x' 
                                                    FROM slrrasg 
                                                    WHERE slrrasg_pidm = starrez_extract.id4
                                                         AND slrrasg_begin_date = CheckInDate
                                                         and slrrasg_end_date = checkoutdate
                                                         AND slrrasg_ascd_code = 'AC'
                                                         and customstring11 = slrrasg_bldg_code
                                                         AND decode(LENGTH(CustomString2),1,lpad(CustomString2,2,0),CustomString2) = slrrasg.slrrasg_room_number
                                                )
                and trim(termsessioncode) <> 'NOTERM'         
        then 'Y'
        else null
        END) MISSING_MATCH_SLRRASG_RECORD
FROM  starrez_bookings_extract_vw starrez_extract 
WHERE id4 <> 0
AND (sysdate + 1 BETWEEN checkindate AND checkoutdate
            OR sysdate + 2 BETWEEN checkindate AND checkoutdate
            OR sysdate + 3 BETWEEN CheckInDate AND CheckOutDate)
and  (CASE starrez_extract.abbreviation
                        WHEN 'RESV' 
                            THEN 'AC'
                        WHEN 'INRM' 
                            THEN 'AC'
                        WHEN 'CNCL' 
                            THEN 'CA'
                        WHEN 'HIST' 
                            THEN 'MO'
        END) = 'AC'       
and trim(TermSessionCode) is not null
ORDER BY 1;