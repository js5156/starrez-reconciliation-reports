create view STARREZ_BOOKING_EXTRACT as 
SELECT
  [Entry_Booking_Entry_lkp].ID4,
  [Entry_Booking_TermSession_lkp].TermSessionCode as TERMSESSIONCODE,
  [Entry_Booking].CheckInDate as CHECKINDATE,
  [Entry_Booking].CheckOutDate as CHECKOUTDATE,
  [Entry_Booking_RoomSpace_lkp_RoomBase_lkp].CustomString1 AS CUSTOMSTRING12,
  [Entry_Booking_RoomLocation_lkp].CustomString1 AS CUSTOMSTRING11,
  [Entry_Booking_RoomSpace_lkp_RoomBase_lkp].CustomString2 as CUSTOMSTRING2,
  [Entry_Booking_RoomSpace_lkp].Street as STREET,
  [Entry_Booking_RoomSpace_lkp].Street2 AS STREET2,
  [Entry_Booking_RoomLocation_lkp].City as CITY,
  [Entry_Booking_RoomLocation_lkp].ZipPostcode as ZIPPOSTCODE,
  [Entry_Booking_RoomLocation_lkp].StateProvince as STATEPROVINCE,
  [Entry_Booking_EntryStatusEnum_lkp].Abbreviation as ABBREVIATION,
  [Entry_Booking].DateModified as DATEMODIFIED,
  [Entry_Booking].EntryStatusEnum as ENTRYSTATUSENUM,
  [Entry_Booking].TermSessionID as TERMSESSIONID,
  [Entry_Booking].DateCreated as DATECREATED,
  [Entry_Booking].BookingID as BOOKINGID
FROM dbo.[Entry]
INNER JOIN dbo.[Booking] [Entry_Booking]
  ON (([Entry].EntryID = [Entry_Booking].EntryID))
INNER JOIN [snRez0_Main].dbo.[Entry] [Entry_Booking_Entry_lkp]
  ON (([Entry_Booking].EntryID = [Entry_Booking_Entry_lkp].EntryID))
INNER JOIN [snRez0_Main].dbo.[TermSession] [Entry_Booking_TermSession_lkp]
  ON (([Entry_Booking].TermSessionID = [Entry_Booking_TermSession_lkp].TermSessionID))
INNER JOIN [snRez0_Main].dbo.[RoomSpace] [Entry_Booking_RoomSpace_lkp]
  ON (([Entry_Booking].RoomSpaceID = [Entry_Booking_RoomSpace_lkp].RoomSpaceID))
INNER JOIN [snRez0_Main].dbo.[RoomBase] [Entry_Booking_RoomSpace_lkp_RoomBase_lkp]
  ON (([Entry_Booking_RoomSpace_lkp].RoomBaseID = [Entry_Booking_RoomSpace_lkp_RoomBase_lkp].RoomBaseID))
INNER JOIN [snRez0_Main].dbo.[RoomLocation] [Entry_Booking_RoomLocation_lkp]
  ON (([Entry_Booking].RoomLocationID = [Entry_Booking_RoomLocation_lkp].RoomLocationID))
INNER JOIN dbo.[EntryStatusEnum] [Entry_Booking_EntryStatusEnum_lkp]
  ON (([Entry_Booking].EntryStatusEnum = [Entry_Booking_EntryStatusEnum_lkp].EntryStatusEnum))
WHERE (((([Entry_Booking].EntryStatusEnum IN (2, 5, 10, 70)))))
